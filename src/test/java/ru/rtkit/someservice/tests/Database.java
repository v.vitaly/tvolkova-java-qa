package ru.rtkit.someservice.tests;

import org.junit.jupiter.api.Test;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Database {

    String pathToDB = getClass().getClassLoader().getResource("sqlite/chinook.db").getPath();

    String dbUrl = "jdbc:sqlite:" + pathToDB;

    @Test
    void get(){

        String query = "SELECT * FROM Playlists LIMIT 1";
        List<String> playlists = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(dbUrl);
            PreparedStatement ps = conn.prepareStatement(query);
            ResultSet rs = ps.executeQuery()){

            while (rs.next()) {
                playlists.add(rs.getString("name"));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println(playlists);

    }

    @Test
    void get_1(){

        String query = "SELECT * FROM Tracks JOIN Genres ON Tracks.GenreID = Genres.GenreID WHERE Genres.Name = 'Pop'";
        List<String> tracks = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()){

            while (rs.next()) {
                tracks.add(rs.getString("name"));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println(tracks);

    }

}
