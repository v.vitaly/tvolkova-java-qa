package ru.rtkit.someservice.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.rtkit.someservice.apiHelper.Endpoints;

@DisplayName("Наш особенный тестовый класс")
public class SomeTest extends BaseTest {
    @Test
    @DisplayName("Тест получения зверушки")
    @Description("Тестирование метода petstore с созданием и получением зверушки")
    void findPetByID() {
        String originalName = "Пушок";
        String id = createNewPet(originalName);

        String name = apiHelper.get(Endpoints.PET_ID, resp200, id)
                .jsonPath()
                .getString("name");

        Assertions.assertEquals(originalName, name);
    }

    @Step("Создание новой зверушки")
    String createNewPet(String name) {
        JSONObject body = new JSONObject()
                .put("id", 0)
                .put("category", new JSONObject().put("name", "серые кошки"))
                .put("name", name)
                .put("status", "available");

        return apiHelper.post(Endpoints.NEW_PET, body.toString(), resp200)
                .jsonPath()
                .getString("id");
    }
}
