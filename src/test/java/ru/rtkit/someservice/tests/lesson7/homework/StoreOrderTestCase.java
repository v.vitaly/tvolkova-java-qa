package ru.rtkit.someservice.tests.lesson7.homework;

import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class StoreOrderTestCase {
    @Test
    void createOrder() {
        StoreOrderApiRequest request = new StoreOrderApiRequest();
        request.setId(25);
        request.setPetId(125);
        request.setQuantity(1);
        request.setShipDate("2022-08-23T17:40:52.647Z");
        request.setStatus("placed");
        request.setComplete(true);

        given()
                .when()
                .body(request)
                .contentType(ContentType.JSON)
                .post("https://petstore.swagger.io/v2/store/order")
                .then()
                .statusCode(200);
    }

    @Test
    void createOrderWithLombok() {
        StoreOrderApiRequestLombok request = new StoreOrderApiRequestLombok();
        request.setId(25);
        request.setPetId(125);
        request.setQuantity(1);
        request.setShipDate("2022-08-23T17:40:52.647Z");
        request.setStatus("placed");
        request.setComplete(true);

        given()
                .when()
                .body(request)
                .contentType(ContentType.JSON)
                .post("https://petstore.swagger.io/v2/store/order")
                .then()
                .statusCode(200);
    }
}
