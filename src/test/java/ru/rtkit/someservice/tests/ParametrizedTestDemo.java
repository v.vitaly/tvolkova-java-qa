package ru.rtkit.someservice.tests;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ParametrizedTestDemo {
    @ParameterizedTest
    @CsvSource({
            "meat,     4",
            "qivi,     8",
            "icecream, 700_00"
    })
    void testWithCsvSource(String fruit, int rank){
        assertNotNull(fruit);
        assertNotEquals(0, rank);
    }

    //использование csv значений в качестве аргумента тестовой функции
    @ParameterizedTest
    @CsvFileSource(resources = "/country-reference.csv", numLinesToSkip = 1)
    void testWithCsvFileSourceFromClassPath(String country, int reference){
        System.out.println(country+" "+reference);
        assertNotNull(country);
        assertNotEquals(0, reference);
    }

}
