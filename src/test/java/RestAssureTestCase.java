import io.restassured.http.ContentType;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;

public class RestAssureTestCase {
    @Test
     void postOrder1(){
        String body = "{\n" +
                "  \"id\": 25,\n" +
                "  \"name\": catsName,\n" +
                "  \"status\": \"available\",\n" +
                "}";

        String catsName = "<Муся>";
        JSONObject bodyJO = new JSONObject()
                .put("id", 25)
                .put("name", catsName)
                .put("status", "available");

        given()
                .when()
                .body(bodyJO.toString())
                .contentType(ContentType.JSON)
                .post("https://petstore.swagger.io/v2/store/order")
                .then()
                .statusCode(200);
    }

    @Test
    void deleteOrder1() {
        given()
                .when()
                .pathParam("orderId", 25)
                .delete("https://petstore.swagger.io/v2/store/order/{orderId}")
                .then()
                .statusCode(200);
    }

}
