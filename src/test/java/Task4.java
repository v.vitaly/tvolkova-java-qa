import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.anyOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

//Case 1 Параметризация тестов, валидация полей
//Case 2 Использование Junit ассертов
//Case 3 работа с JSONObject (вложенными полями)
public class Task4 {
    @ParameterizedTest
    @ValueSource(strings = {"Busya", "BUSYA", "busya", "BuSyA", "Буся", "БУСЯ", "буся", "БуСя"})
    void createPetWithDifferentNames(String petName) {
        JSONObject bodyJO = new JSONObject()
                .put("name", petName)
                .put("status", "available");
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(bodyJO.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200)
                .assertThat()
                .body("$", Matchers.hasKey("name"))
                .body("name", Matchers.equalTo(petName));
    }

    @Test
    void createOrder() {
        JSONObject bodyJO = new JSONObject()
                .put("name", "Pushok");
        long petId = given()
                .when()
                .contentType(ContentType.JSON)
                .body(bodyJO.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200)
                .extract()
                .path("id");

        bodyJO = new JSONObject()
                .put("id", 0)
                .put("petId", petId)
                .put("quantity", 1)
                .put("shipDate", "2022-07-29T20:35:14.206Z")
                .put("status", "placed")
                .put("complete", true);
        Response response = given()
                .when()
                .contentType(ContentType.JSON)
                .body(bodyJO.toString())
                .post("https://petstore.swagger.io/v2/store/order")
                .then()
                .statusCode(200)
                .extract()
                .response();

        assertEquals(petId, (Long) response.path("petId"));
        assertThat(
                response.path("status"),
                anyOf(is("placed"), is("approved"), is("delivered"))
        );
        assertEquals(true, response.path("complete"));
        assertNotEquals("", response.path("shipDate"));
    }

    @Test
    void createPetWithComplexData() {
        JSONObject category = new JSONObject()
                .put("id", 1)
                .put("name", "кошка");
        JSONArray photoUrls = new JSONArray()
                .put("https://example.com/pic_1")
                .put("https://example.com/pic_2");
        JSONArray tags = new JSONArray()
                .put(new JSONObject().put("id", 1).put("name", "серые кошки"))
                .put(new JSONObject().put("id", 2).put("name", "породистые кошки"));
        JSONObject bodyJO = new JSONObject()
                .put("category", category)
                .put("name", "Пушок")
                .put("photoUrls", photoUrls)
                .put("tags", tags)
                .put("status", "available");

        given()
                .when()
                .body(bodyJO.toString())
                .contentType(ContentType.JSON)
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .statusCode(200);
    }

}
