import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SomeClassTestCase {
    public static Logger LOGGER = LoggerFactory.getLogger(SomeClassTestCase.class);
    @Test
    public void LogExample() {
        LOGGER.info("Это сообщение будет выведено в лог");
    }

}
